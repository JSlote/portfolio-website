// var mozjpeg = require('imagemin-mozjpeg');

module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    clean: {
      all: ['dist/**/*.html']
    },
    connect: {
      dev: {
        options: {
          port: 8080,
          base: 'dist/',
          keepalive: true
        }
      }
    },
    sass: {
      dist: {
        options: {
          style: 'compressed'
        },
        files: {
          'dist/style/main.css': 'src/style/main.scss'
        }
      }
    },
    image_resize: {
      resize: {
        options: {
          width: 1500,
        },
        files:[{
          expand:true,
          cwd:'temp/images/',
          src:['**/*.jpg'],
          dest: 'dist/images/'
        }]
      }
    },
    imagemin: {
      dynamic: {
        options: {
          // optimizationLevel: 3
          // use: [mozjpeg()],
          cache:false
        },
        files: [{
          expand:true,
          cwd:'src/assets/images/',
          src: ['**/*.jpg'],
          dest: 'temp/images/'
        }]
      }
    },
    assemble: {
      options:{
        layoutdir: 'src/layouts/',
        partials: 'src/partials/**/*.hbs',
        flatten: true,
        helpers: './src/helpers/**/*.js',
        collections: [{
          name: 'project',
          sortby: 'date',
          sortorder: 'ascending'
        }]
      },
      projects: {
        options: {
          layout: 'project.hbs'
        },
        files: {
          'dist/projects/':['src/pages/projects/*.hbs'],
          'dist/':['src/pages/index.hbs']
        }
      }
    }
  });

  // Default task(s).
  grunt.loadNpmTasks('assemble');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-image-resize');
  grunt.registerTask('default', ['clean', 'sass', 'assemble','imagemin','image_resize','connect']);
  grunt.registerTask('code', ['clean', 'sass', 'assemble','connect']);
  grunt.registerTask('image', ['imagemin','image_resize']);
};
